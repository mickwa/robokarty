﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CardsController : MonoBehaviour
{
  CardController[] _cardSlots;

  [Inject]
  CardManager _cardManager;

  [Inject]
  IPlayerModel playerModel;

  private void Start()
  {
    _cardSlots = GetComponentsInChildren<CardController>();
    UpdateCards();
    RegisterEvents();
  }

  private void OnDestroy()
  {
    UnregisterEvents();
  }
  private void RegisterEvents()
  {
    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
    _cardManager.OnUpdateCards += _cardManager_OnUpdateCards;
  }


  private void UnregisterEvents()
  {
    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
    _cardManager.OnUpdateCards -= _cardManager_OnUpdateCards;
  }

  private void PlayerModel_OnGameEnd(EndGame obj)
  {
    gameObject.SetActive(false);
  }

  private void _cardManager_OnUpdateCards()
  {
    UpdateCards();
  }

  void UpdateCards()
  {
    List<Card> newSelectedCards = _cardManager.SelectCards(3);
    for (int i = 0; i < _cardSlots.Length; i++)
    {
      _cardSlots[i].UpdateCard(newSelectedCards[i]);
    }
  }
}