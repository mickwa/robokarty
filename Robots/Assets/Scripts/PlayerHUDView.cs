﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerHUDView : MonoBehaviour
{
  [SerializeField]
  Canvas canvas;

  [SerializeField]
  GameObject ProgressBarElementPrefab;

  [SerializeField]
  ProgressBarController MoneyProgress;

  [SerializeField]
  ProgressBarController CoolProgress;

  [SerializeField]
  ProgressBarController DanceProgress;

  [SerializeField]
  ProgressBarController AlcoProgress;

  [Inject]
  IPlayerModel playerModel;

  public static float _canvasScale;
  public static float canvasScale { get { return _canvasScale; } }

  private void Awake()
  {
    _canvasScale = canvas.transform.lossyScale.x;
  }

  private void Start()
  {
    CreateProgressBars();
    RegisterEvents();
  }

  private void OnDestroy()
  {
    UnregisterEvents();
  }

  private void CreateProgressBars()
  {
    MoneyProgress.AddFields(PlayerModel.MaxMoney);
    MoneyProgress.SetValue(playerModel.Money);

    CoolProgress.AddFields(PlayerModel.MaxCoolness);
    CoolProgress.SetValue(playerModel.Coolness);

    DanceProgress.AddFields(PlayerModel.MaxDance);
    DanceProgress.SetValue(playerModel.Dance);

    AlcoProgress.AddFields(PlayerModel.MaxAlco);
    AlcoProgress.SetValue(playerModel.Alco);
  }



  private void RegisterEvents()
  {
    playerModel.OnMoneyChanged += PlayerModel_OnMoneyChanged;
    playerModel.OnAlcoChanged += PlayerModel_OnAlcoChanged;
    playerModel.OnDanceChanged += PlayerModel_OnDanceChanged;
    playerModel.OnCoolnessChanged += PlayerModel_OnCoolnessChanged;

    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
  }

  private void PlayerModel_OnGameEnd(EndGame obj)
  {
    //foreach(Transform t in transform)
    //{
    //  t.gameObject.SetActive(false);
    //}
  }

  private void PlayerModel_OnMoneyChanged()
  {
    MoneyProgress.SetValue(playerModel.Money);
  }

  private void PlayerModel_OnCoolnessChanged()
  {
    CoolProgress.SetValue(playerModel.Coolness);
  }

  private void PlayerModel_OnDanceChanged()
  {
    DanceProgress.SetValue(playerModel.Dance);
  }

  private void PlayerModel_OnAlcoChanged()
  {
    AlcoProgress.SetValue(playerModel.Alco);
  }

  private void UnregisterEvents()
  {
    playerModel.OnMoneyChanged -= PlayerModel_OnMoneyChanged;
    playerModel.OnAlcoChanged -= PlayerModel_OnAlcoChanged;
    playerModel.OnDanceChanged -= PlayerModel_OnDanceChanged;
    playerModel.OnCoolnessChanged -= PlayerModel_OnCoolnessChanged;

    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
  }
}