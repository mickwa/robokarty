﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public interface IPlayerModel
{
  int Money { get; set; }
  int Coolness { get; set; }
  int Dance { get; set; }
  int Alco { get; set; }
  event Action OnMoneyChanged;
  event Action OnCoolnessChanged;
  event Action OnDanceChanged;
  event Action OnAlcoChanged;
  event Action<EndGame> OnGameEnd;

  void GrantRewards(CardRewards rewards);
  void ResetValues();

}
