﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LightsController : MonoBehaviour
{
  [Inject]
  IPlayerModel playerModel;

  [SerializeField]
  Light[] spotlights;

  [SerializeField]
  Light directionalLight;
  
  private void Start()
  {
    RegisterEvents();
  }

  private void OnDestroy()
  {
    UnregisterEvents();
  }
  void RegisterEvents()
  {
    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
  }

  private void PlayerModel_OnGameEnd(EndGame obj)
  {
    if(obj == EndGame.DANCE)
    {
      directionalLight.intensity = 5;
      StartDisco();
    }
  }

  void ChangeLightColors()
  {
    directionalLight.color = GetRandomColor();
    foreach(Light l in spotlights )
    {
      l.color = GetRandomColor();
      Vector3 ang = l.transform.rotation.eulerAngles;
      ang = new Vector3(ang.x, Random.Range(0, 360), ang.z);
      Quaternion q = new Quaternion();
      q.eulerAngles = ang;
      l.transform.rotation = q;
    }
  }

  private void StartDisco()
  {
    InvokeRepeating("ChangeLightColors", 0, 1);
  }

  void UnregisterEvents()
  {
    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
  }

  Color GetRandomColor()
  {
    return new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f), 1f);
  }
}
