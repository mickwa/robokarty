﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using TMPro;

public class RestartGameButtonController : MonoBehaviour
{
  [Inject]
  IPlayerModel playerModel;

  [SerializeField]
  TextMeshProUGUI EndGameInfo;

  void Start()
  {
    RegisterEvents();
    gameObject.SetActive(false);
  }

  private void OnDestroy()
  {
    UnregisterEvents();
  }

  public void RestartGame()
  {
    playerModel.ResetValues();
    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
  }


  public void LoadMainMenu()
  {
    SceneManager.LoadScene("IntroScene");
  }

  private void RegisterEvents()
  {
    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
  }


  private void UnregisterEvents()
  {
    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
  }

  private void PlayerModel_OnGameEnd(EndGame obj)
  {
    gameObject.SetActive(true);

    EndGameInfo.text = "";

    switch (obj)
    { case (EndGame.DANCE):
        {
          EndGameInfo.text = "Zostajesz królem parkietu!";
          break;
        } case (EndGame.DANCE_0):
        {
          EndGameInfo.text = "Tańczysz tak fatalnie, że ochorniarz wywala cię z klubu";
          break;
        }
      case (EndGame.ALCO):
        {
          EndGameInfo.text = "Zaliczasz zgon. Takie są skutki picia WD 40 i Shella ;)";
          break;
        }
      case (EndGame.ALCO_0):
        {
          EndGameInfo.text = "Jesteś trzeźwy, a to miejsce jest zbyt fatalne aby przebywać tu na trzeźwo...";
          break;
        }
     
      case (EndGame.MONEY):
        {
          EndGameInfo.text = "Hajs się zgadza";
          break;
        }
     
      case (EndGame.MONEY_0):
        {
          EndGameInfo.text = "Nie masz kasy więc jesteś nikim. Wypad z baru.";
          break;
        }
      case (EndGame.COOLNESS):
        {
          EndGameInfo.text = "Jesteś tak cool, że nawet panny się do ciebie kleją.";
          break;
        }
      case (EndGame.COOLNESS_0):
        {
          EndGameInfo.text = "Denerwujesz wszystkich. DJa, barmana i ochroniarza. Masz 3 sekundy żeby się zmywać.";
          break;
        }
     
    }
  }
}