﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
  [SerializeField]
  GameObject ButtonsMenu;

  [SerializeField]
  GameObject AboutGame;

  [SerializeField]
  GameObject AboutAuthor;

  public void ShowButtonsMenu()
  {
    ButtonsMenu.SetActive(true);
    AboutGame.SetActive(false);
    AboutAuthor.SetActive(false);
  }

  public void ShowAboutAuthor()
  {
    ButtonsMenu.SetActive(false);
    AboutGame.SetActive(false);
    AboutAuthor.SetActive(true);
  }

  public void ShowAboutGame()
  {
    ButtonsMenu.SetActive(false);
    AboutGame.SetActive(true);
    AboutAuthor.SetActive(false);
  }

  public void StartGame()
  {
    UnityEngine.SceneManagement.SceneManager.LoadScene("PubScene");
  }

  public void ExitGame()
  {
    Application.Quit();
  }
}
