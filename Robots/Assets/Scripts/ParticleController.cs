﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ParticleController : MonoBehaviour
{
  new ParticleSystem particleSystem;

  [Inject]
  IPlayerModel playerModel;

  private void Start()
  {
    particleSystem = GetComponent<ParticleSystem>();
    particleSystem.Stop();
    RegisterEvents();
  }

  private void RegisterEvents()
  {
    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
  }


  private void UnregisterEvents()
  {
    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
  }

  private void OnDestroy()
  {
    UnregisterEvents();
  }
  private void PlayerModel_OnGameEnd(EndGame obj)
  {
    if (obj == EndGame.MONEY)
    {
      particleSystem.Play();
    }
  }
}
