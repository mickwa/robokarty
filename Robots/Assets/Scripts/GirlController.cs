﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GirlController : MonoBehaviour
{
  [Inject]
  IPlayerModel playerModel;

  [SerializeField]
  bool isStatic;

  void Awake()
  {
    RegisterEvents();
    gameObject.SetActive(!isStatic);
  }
  
  private void OnDestroy()
  {
    UnregisterEvents();
  }

  void RegisterEvents()
  {
    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
  }

  void UnregisterEvents()
  {
    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
  }

  private void PlayerModel_OnGameEnd(EndGame obj)
  {

    if (obj == EndGame.COOLNESS)
    {
      if (isStatic == false)
      {
        transform.position = new Vector3(-21.73f, 0, -5);
      }
      else
      {
        gameObject.SetActive(true);
      }
    }
  }
}