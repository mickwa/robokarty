using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller<GameInstaller>
{
  public ProgressElementContol prefab;

  public override void InstallBindings()
  {
    Debug.Log("Install Bindings start");
    Container.Bind<IPlayerModel>().To<PlayerModel>().AsSingle();
    Container.Bind<CardManager>().AsSingle();
    Container.BindFactory<ProgressElementContol, ProgressElementContol.ElementFactory>().FromComponentInNewPrefab(prefab);
    Debug.Log("Install Bindings end");
  }
}