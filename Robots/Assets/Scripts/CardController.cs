﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;

public class CardController : MonoBehaviour
{

  [SerializeField]
  TMP_Text title;

  [SerializeField]
  TMP_Text descirption;

  [SerializeField]
  TMP_Text AlcoValue;

  [SerializeField]
  TMP_Text DanceValue;

  [SerializeField]
  TMP_Text MoneyValue;

  [SerializeField]
  TMP_Text CoolnessValue;


  [SerializeField]
  GameObject AlcoReward;
  [SerializeField]
  GameObject DanceReward;
  [SerializeField]
  GameObject MoneyReward;
  [SerializeField]
  GameObject CoolnessReward;

  private Card _card;

  [Inject]
  IPlayerModel playerModel;

  [Inject]
  CardManager cardManager;

  private string redColor = "<#bb1111>";
  private string greenColor = "<#11bb11>";

  public void CardSelected()
  {
    playerModel.GrantRewards(_card.Rewards);
    cardManager.UpdateCardsRequest();
  }

  public void UpdateCard(Card newCard)
  {
    _card = newCard;
    title.text = _card.Title;
    descirption.text = _card.Description;

    AlcoReward.SetActive(_card.Rewards.Alco != 0);
    AlcoValue.text = Colored(_card.Rewards.Alco);

    DanceReward.SetActive(_card.Rewards.Dance != 0);
    DanceValue.text = Colored(_card.Rewards.Dance);

    CoolnessReward.SetActive(_card.Rewards.Coolness != 0);
    CoolnessValue.text = Colored(_card.Rewards.Coolness);

    MoneyReward.SetActive(_card.Rewards.Money != 0);
    MoneyValue.text = Colored(_card.Rewards.Money);
  }

  private string Colored(int v)
  {
    return (v > 0 ? greenColor : redColor) + (v > 0 ? "+" : "") + v + "</color>";
  }
}
