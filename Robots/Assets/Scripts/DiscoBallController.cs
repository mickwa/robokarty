﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class DiscoBallController : MonoBehaviour
{
  [Inject]
  IPlayerModel playerModel;

  void Start()
  {
    RegisterEvents();
  }

  // Update is called once per frame
  void Update()
  {
    transform.Rotate(new Vector3(0, 0, -100 * Time.deltaTime));
  }
  private void OnDestroy()
  {
    UnregisterEvents();
  }

 

  private void PlayerModel_OnGameEnd(EndGame obj)
  {
    if (obj == EndGame.DANCE)
    {
      GetComponent<Animator>().SetTrigger("MoveDiscoBallDown");
    }
  }

  void RegisterEvents()
  {
    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
  }
  void UnregisterEvents()
  {
    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
  }
}
