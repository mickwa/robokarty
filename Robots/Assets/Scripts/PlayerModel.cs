﻿using System;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine;

public enum EndGame
{
  DANCE,
  DANCE_0,
  ALCO,
  ALCO_0,
  COOLNESS,
  COOLNESS_0,
  MONEY,
  MONEY_0
}

public class PlayerModel : IPlayerModel
{
  public static  int MaxMoney = 14;
  public static  int MaxCoolness = 10;
  public static  int MaxAlco = 10;
  public static  int MaxDance = 12;


  public event Action OnMoneyChanged;
  public event Action OnCoolnessChanged;
  public event Action OnDanceChanged;
  public event Action OnAlcoChanged;
  public event Action<EndGame> OnGameEnd;

  private int _money;

  bool gameInProgress = true;

  private Random rand;

  public int Money
  {
    get { return _money; }
    set
    {
      if (_money != value) 
      {
        _money = value;
        if (OnMoneyChanged != null) OnMoneyChanged();
        CheckForGameEnd();
      }
    }
  }

  private int _coolness;
  public int Coolness
  {
    get
    {
      return _coolness;
    }

    set
    {
      if (_coolness != value)
      {
        _coolness = value;
        if (OnCoolnessChanged != null) OnCoolnessChanged();
        CheckForGameEnd();
      }
    }
  }

  private int _dance;
  public int Dance
  {
    get
    {
      return _dance;
    }

    set
    {
      if (_dance != value)
      {
        _dance = value;
        if (OnDanceChanged != null) OnDanceChanged();
        CheckForGameEnd();
      }
    }
  }

  private int _alco;
  public int Alco
  {
    get
    {
      return _alco;
    }

    set
    {
      if (_alco != value)
      {
        _alco = value;
        if (OnAlcoChanged != null) OnAlcoChanged();
        CheckForGameEnd();
      }
    }
  }

  public PlayerModel()
  {
    rand = new Random();
    ResetValues();
  }

  int v()
  {
    int i;
    i = rand.Next(-2, 3);
    UnityEngine.Debug.Log(i);
    return i;
  }
  public void ResetValues()
  {
    MaxAlco = rand.Next(5, 8) * 2 + rand.Next(0,2);
    MaxMoney = rand.Next(5, 8) * 2 + rand.Next(0,2);
    MaxDance = rand.Next(5, 8) * 2 + rand.Next(0,2);
    MaxCoolness = rand.Next(5, 8) * 2 + rand.Next(0,2);

    Money = MaxMoney / 2 + v();
    Alco = MaxAlco / 2 + v();
    Dance = MaxDance / 2 + v();
    Coolness = MaxCoolness / 2 + v();
    //Money = MaxMoney -1;
    //Alco = MaxAlco - 1;
    //Dance = MaxDance - 1;
    //Coolness = MaxCoolness - 1;
    gameInProgress = true;
  }

  public void GrantRewards(CardRewards rewards)
  {
    Money += rewards.Money;
    Alco += rewards.Alco;
    Coolness += rewards.Coolness;
    Dance += rewards.Dance;
  }

  
  private void CheckForGameEnd()
  {
    if (gameInProgress)
    {
      gameInProgress = false;
      if (Dance >= MaxDance)
        if (OnGameEnd != null) { OnGameEnd(EndGame.DANCE); return; }
      if (Dance <= 0)
        if (OnGameEnd != null) { OnGameEnd(EndGame.DANCE_0); return; }

      if (Alco >= MaxAlco)
        if (OnGameEnd != null) { OnGameEnd(EndGame.ALCO); return; }
      if (Alco <= 0)
        if (OnGameEnd != null) { OnGameEnd(EndGame.ALCO_0); return; }

      if (Money >= MaxMoney)
        if (OnGameEnd != null) { OnGameEnd(EndGame.MONEY); return; }
      if (Money <= 0)
        if (OnGameEnd != null) { OnGameEnd(EndGame.MONEY_0); return; }

      if (Coolness >= MaxCoolness)
        if (OnGameEnd != null) { OnGameEnd(EndGame.COOLNESS); return; }
      if (Coolness <= 0)
        if (OnGameEnd != null) { OnGameEnd(EndGame.COOLNESS_0); return; }

      gameInProgress = true;
    }

  }

  ~PlayerModel()
  {
  //  Debug.Log("Player Model dtor()"); // Lets check when Zenject is destroying this.
  }
}