﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class RobotController : MonoBehaviour
{
  [Inject]
  IPlayerModel playerModel;

  public Material DanceMaterial;
  public Material GoldMaterial;

  [SerializeField]
  GameObject glasses;

  SkinnedMeshRenderer r;
  Animator anim;

  void Awake()
  {
    r = GetComponentInChildren<SkinnedMeshRenderer>();
    anim = GetComponentInChildren<Animator>();
    
    playerModel.ResetValues();
    RegisterEvents();
  }
  
  private void OnDestroy()
  {
    UnregisterEvents();
  }
  
  private void PlayerModel_OnGameEnd(EndGame obj)
  {
    if (obj == EndGame.DANCE)
    {
      Material[] mts = r.materials;
      mts[0] = DanceMaterial;
      r.materials = mts;
      anim.SetTrigger("StartDancing");
    }

    if(obj == EndGame.MONEY)
    {
      Material[] mts = r.materials;
      mts[0] = GoldMaterial;
      mts[1] = GoldMaterial;
      mts[2] = GoldMaterial;
      mts[3] = GoldMaterial;
      mts[4] = GoldMaterial;
      r.materials = mts;
      anim.SetTrigger("MoneyGameEnd");
    }

    if(obj == EndGame.ALCO)
    {
      anim.SetTrigger("DrunkGameEnd");
    }

    if(obj == EndGame.COOLNESS)
    {
      glasses.SetActive(true);
    }
  }

  void RegisterEvents()
  {
    playerModel.OnGameEnd += PlayerModel_OnGameEnd;
  }

  void UnregisterEvents()
  {
    playerModel.OnGameEnd -= PlayerModel_OnGameEnd;
  }
}
