﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Zenject;

public class ProgressBarController : MonoBehaviour
{
  [Inject]
  ProgressElementContol.ElementFactory factory;

  List<ProgressElementContol> _controls;

  public void Awake()
  {
    _controls = new List<ProgressElementContol>();
  }

  public void AddFields(int fieldsAmount)
  {
    for (int i = 0; i < fieldsAmount; i++)
    {
      ProgressElementContol e = factory.Create();
      e.transform.SetParent(transform);
      e.name = "e" + " " + i;
      _controls.Add(e);
    }
  }

  public void SetValue(int v)
  {
    _controls.Take(v).ToList().ForEach(e => e.SetValue(true));
    _controls.Skip(v).ToList().ForEach(e => e.SetValue(false));
  }
}
