﻿using System.Collections;
using System.Collections.Generic;
using System;

public static class HelperFunctions
{
  public static void Shuffle<T>(this List<T> list)
  {
    Random r = new Random();
    for(int i=0;i<list.Count;i++)
    {
      int index1 = r.Next(0, list.Count);
      int index2 = r.Next(0, list.Count);
      T tmp = list[index1];
      list[index1] = list[index2];
      list[index2] = tmp;
    }
  }
}