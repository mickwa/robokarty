﻿using System;
using System.Collections.Generic;
using System.Linq;

public partial class CardManager
{
  private List<Card> _allCards;
  private List<int> _indexes;
 
  public event Action OnUpdateCards;

  public CardManager()
  {
    CreateAllCards();
    CreateRandomGenerator();
    TestCards();
  }

  public void UpdateCardsRequest()
  {
    if (OnUpdateCards != null) OnUpdateCards();
  }

  public List<Card> SelectCards(int amount)
  {
    List<int> selectedIndexes = NewSelectedCardIndexes(amount);
    List<Card> selectedCards = new List<Card>();
    foreach (int i in selectedIndexes)
    {
      selectedCards.Add(_allCards[i]);
    }
    return selectedCards;
  }

  

  private void CreateRandomGenerator()
  {
    _indexes = new List<int>();
    for (int i = 0; i < _allCards.Count; i++)
    {
      _indexes.Add(i);
    }
  }

  private List<int> NewSelectedCardIndexes(int amount)
  {
    _indexes.Shuffle();
    return _indexes.Take(amount).ToList();
  }
}

public class Card
{
  public string Title;
  public string Description;
  public CardRewards Rewards;
}

public class CardRewards
{
  public int Alco, Coolness, Dance, Money;
}

