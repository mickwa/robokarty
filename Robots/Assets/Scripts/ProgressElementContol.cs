﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Image))]
public class ProgressElementContol : MonoBehaviour
{
  private Image sprite;
  private static readonly Color FilledColor = new Color(0.1f, 1, 0.1f, 0.8f);
  private static readonly Color EmptyColor = new Color(0.6f, 0.6f, 0.6f, 0.6f);

  void Awake()
  {
    sprite = GetComponent<Image>();
    // This is needed because canvas has scalewithscreen size, and canvas scale is not 1
    transform.localScale *= PlayerHUDView.canvasScale;
  }
  public void SetValue(bool filled)
  {
    sprite.color = filled ? FilledColor : EmptyColor;
  }

  public class ElementFactory:Factory<ProgressElementContol>
  {
  }
}
