﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public partial class CardManager
{
  private void CreateAllCards()
  {
    _allCards = new List<Card>();
    _allCards.Add(new Card { Title = "Suchar na puchar", Description = "Gadasz suchary do ochroniarza \n\n <i>Ile to jest 8 Hobbitów? 1 Hobbajt</i>", Rewards = new CardRewards { Alco = 0, Coolness = -1, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Salsa", Description = "Przypomniałeś sobie jakieś ruchy z kursu salsy sprzed 4 lat", Rewards = new CardRewards { Alco = 0, Coolness = 1, Dance = 2, Money = 0 } });
    _allCards.Add(new Card { Title = "Psycho ex", Description = "Spotykasz psycho ex. Robi ci sceny na środku klubu, ale udaje ci się uciec.", Rewards = new CardRewards { Alco = 0, Coolness = -2, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Zgon", Description = "Znalazłeś wolne miejsce i siadasz na chwilkę, żeby lekko wytrzeźwieć", Rewards = new CardRewards { Alco = -1, Coolness = -2, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Ziomeczki", Description = "Spotkałeś kolesi z akademika. Stawiasz kufelek Shella", Rewards = new CardRewards { Alco = 2, Coolness = 1, Dance = 0, Money = -3 } });
    _allCards.Add(new Card { Title = "Zmieszałeś...", Description = "Zmieszałeś kufelek Shella z WD 40. \n\n <i> \"A mama mówiła nie mieszaj...\" </i>", Rewards = new CardRewards { Alco = 2, Coolness = -1, Dance = 0, Money = -3 } });
    _allCards.Add(new Card { Title = "Sęp", Description = "Jakiś znajomy się napatoczył i chce pożyczyk kasę.", Rewards = new CardRewards { Alco = 0, Coolness = 1, Dance = 0, Money = -2 } });
    _allCards.Add(new Card { Title = "Sęp 2", Description = "Spotkałeś ziomka z liceum. Pożyczył 2 dychy i postawił browara. Ale jest rudy więc lepiej się zmywać.", Rewards = new CardRewards { Alco = 2, Coolness = -1, Dance = 0, Money = 2 } });
    _allCards.Add(new Card { Title = "Farcik", Description = "Ale fart! Znalazłeś trochę kasy w klubie", Rewards = new CardRewards { Alco = 0, Coolness = 0, Dance = 0, Money = 2 } });
    _allCards.Add(new Card { Title = "Mini Farcik", Description = "Ale fart! Znalazłeś trochę kasy w klubie", Rewards = new CardRewards { Alco = 0, Coolness = 0, Dance = 0, Money = 1 } });
    _allCards.Add(new Card { Title = "Peszek", Description = "Ale peszek! Zgubiłeś 2 dychy w klubie", Rewards = new CardRewards { Alco = 0, Coolness = 0, Dance = 0, Money = -2 } });
    _allCards.Add(new Card { Title = "Peszek na maxa", Description = "Ale peszek! Zgubiłeś sporo kasy w klubie", Rewards = new CardRewards { Alco = 0, Coolness = 0, Dance = 0, Money = -5 } });
    _allCards.Add(new Card { Title = "Zakład", Description = "Założyłeś się o 2 dychy że wypijesz duszkiem duży kufel Shella i wygrałeś", Rewards = new CardRewards { Alco = 3, Coolness = 1, Dance = 0, Money = 2 } });
    _allCards.Add(new Card { Title = "Nieudany Podryw", Description = "Nieudana bajera. Najgorsze że kolesie widzieli...", Rewards = new CardRewards { Alco = 0, Coolness = -3, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Drinki", Description = "Widzisz fajne laski, stawiasz drina, jest jakaś gadka", Rewards = new CardRewards { Alco = 1, Coolness = 2, Dance = 0, Money = -3 } });
    _allCards.Add(new Card { Title = "Z dedykacją", Description = "Zamawiasz piosenkę u DJa. Niestety wybrałeś Krzysztofa Krawczyka. Ale siara...", Rewards = new CardRewards { Alco = 0, Coolness = -2, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Należy się", Description = "Kupujesz sobie kufelek Shella. A co. Należy się.", Rewards = new CardRewards { Alco = 1, Coolness = 0, Dance = 0, Money = -1 } });
    _allCards.Add(new Card { Title = "Skarpety", Description = "Zamiast skarpet Adidas założyłeś Adibasy. Lipa...", Rewards = new CardRewards { Alco = 0, Coolness = -1, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Kocie ruchy", Description = "Wczoraj oglądałeś 'Step up 3'", Rewards = new CardRewards { Alco = 0, Coolness = 2, Dance = 3, Money = 0 } });
    _allCards.Add(new Card { Title = "Kocie ruchy 2", Description = "Jakiś ziomek koło ciebie dobrze tańczy. Próbujesz robić to samo, ale myśli że się do niego podwalasz...", Rewards = new CardRewards { Alco = 0, Coolness = -1, Dance = 2, Money = 0 } });
    _allCards.Add(new Card { Title = "Sztuka melanżu", Description = "To było na melanżu, na na na...", Rewards = new CardRewards { Alco = 2, Coolness = 2, Dance = 2, Money = 0 } });
    _allCards.Add(new Card { Title = "Karaoke", Description = "Przepitym głosem śpiewasz mój piękny cyganie. Ktoś dał ci dychę żebyś się zamknął.", Rewards = new CardRewards { Alco = 0, Coolness = -1, Dance = 0, Money = 1 } });
    _allCards.Add(new Card { Title = "Bohater", Description = "Wciskasz komuś kit że wczoraj uratowałeś kiciusia", Rewards = new CardRewards { Alco = 0, Coolness = 1, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Zadyma", Description = "Dostałeś w głowę taboretem.", Rewards = new CardRewards { Alco = -2, Coolness = -1, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Trans", Description = "Tańczysz na boksera do jakiegoś hitu techno z lat 90.", Rewards = new CardRewards { Alco = -2, Coolness = -1, Dance = 1, Money = 0 } });
    _allCards.Add(new Card { Title = "Kibel", Description = "Pomyliłeś damski z męskim.", Rewards = new CardRewards { Alco = -1, Coolness = -1, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Ochrona 48h", Description = "Tyle miał cie chronić dezodorant. Niestety śmierdzisz już jak skunks po 20 minutach na parkiecie.", Rewards = new CardRewards { Alco = 0, Coolness = -1, Dance = -2, Money = 0 } });
    _allCards.Add(new Card { Title = "Moonwalk", Description = "Oglądałeś teledyski z Michaelm Jacksonem na youtubie.", Rewards = new CardRewards { Alco = 0, Coolness = -1, Dance = 2, Money = 0 } });
    _allCards.Add(new Card { Title = "Śliskooo", Description = "Ślizgasz się na rozlanym Shellu, ale wyglądasz jak Elvis", Rewards = new CardRewards { Alco = 0, Coolness = -1, Dance = 2, Money = 0 } });

    _allCards.Add(new Card { Title = "Jesteś zwycięzcą", Description = "Powtarzasz sobie że jesteś zwycięzcą aby nabrać pewności siebie", Rewards = new CardRewards { Alco = 0, Coolness = 2, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Ktoś stawia", Description = "Ktoś stawia kolejkę.", Rewards = new CardRewards { Alco = 1, Coolness = 2, Dance = 0, Money = 0 } });
    _allCards.Add(new Card { Title = "Trójka", Description = "Wciskasz bajerę, że przyjechałeś trójką, ale Golfem a nie tramwajem.", Rewards = new CardRewards { Alco = 0, Coolness = 2, Dance = 0, Money = 0 } });

    _allCards.Add(new Card { Title = "Zakład", Description = "Założyłeś się, że niezauważony szturchniesz ochroniarza.", Rewards = new CardRewards { Alco = 0, Coolness = 3, Dance = 0, Money = 3 } });
    _allCards.Add(new Card { Title = "Bal Drogowca", Description = "Bal drogowca, Panie proszą panów do walca.", Rewards = new CardRewards { Alco = 0, Coolness = 1, Dance = 2, Money = 0 } });
    _allCards.Add(new Card { Title = "Dresy", Description = "Twoje nowe dresy wyjątkowo dobrze się prezentują na parkiecie\n\n<i>\"Akurat - Dyskoteka gra\" </i>", Rewards = new CardRewards { Alco = 0, Coolness = 1, Dance = 2, Money = 0 } });
  }

  void TestCards()
  {
    int a=0, c=0, d=0, m=0;
    foreach(Card ca in _allCards)
    {
      a += ca.Rewards.Alco;
      c += ca.Rewards.Coolness;
      d += ca.Rewards.Dance;
      m += ca.Rewards.Money;
    }

    UnityEngine.Debug.Log("a: " + a + " c: " + c + " d: " + d + " m: " + m);
  }
}